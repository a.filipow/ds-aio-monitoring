#!/bin/bash

docker swarm init

mkdir -p /etc/traefik/acme

touch /etc/traefik/acme/acme.json

chmod 600 /etc/traefik/acme/acme.json

# create docker networks for main stacks #

docker network create --driver overlay --attachable traefik || true

docker network create --driver overlay --attachable loadbalancer || true

docker network create --driver overlay --attachable dashboard || true

docker network create --driver overlay --attachable monitoring || true

# deploy main stacks #

docker stack deploy --compose-file /ds-aio-monitoring/traefik/docker-compose.yml loadbalancer

echo sleeping for 1m

sleep 1m

docker stack deploy --compose-file /ds-aio-monitoring/portainer/docker-compose.yml dashboard

echo sleeping for 1m

sleep 1m

docker stack deploy --compose-file /ds-aio-monitoring/monitoring/docker-compose.yml monitoring
