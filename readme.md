# monitoring external endpoints (open source)

Ubuntu 18.04 / Docker Swarm

# stack overview
- portainer
- filebrowser
- traefik
- prometheus
- grafana
- nodeexporter
- cadvisor
- blackbox-exporter

# startpoint

- Clean Ubuntu 18.04
- Open Firewall 80,443
- Domain A-Record

# Deployment (#!/bin/bash)

- cd / && git clone https://gitlab.com/a.filipow/ds-aio-monitoring.git
- chmod 700 /ds-aio-monitoring/prepare-node.sh && cd /ds-aio-monitoring && ./prepare-node.sh

configure commented lines
- nano /ds-aio-monitoring/traefik/docker-compose.yml
- nano /ds-aio-monitoring/portainer/docker-compose.yml
- nano /ds-aio-monitoring/monitoring/docker-compose.yml

- chmod 700 /ds-aio-monitoring/create-swarm.sh && cd /ds-aio-monitoring/ && ./create-swarm.sh
